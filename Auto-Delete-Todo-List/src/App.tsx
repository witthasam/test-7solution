import React, { useState, useEffect } from "react";
import "./App.css";

interface Item {
  type: string;
  name: string;
}

const initialData: Item[] = [
  { type: "Fruit", name: "Apple" },
  { type: "Vegetable", name: "Broccoli" },
  { type: "Vegetable", name: "Mushroom" },
  { type: "Fruit", name: "Banana" },
  { type: "Vegetable", name: "Tomato" },
  { type: "Fruit", name: "Orange" },
  { type: "Fruit", name: "Mango" },
  { type: "Fruit", name: "Pineapple" },
  { type: "Vegetable", name: "Cucumber" },
  { type: "Fruit", name: "Watermelon" },
  { type: "Vegetable", name: "Carrot" },
];

const App: React.FC = () => {
  const [itemList, setItemList] = useState<Item[]>(initialData);
  const [fruitList, setFruitList] = useState<Item[]>([]);
  const [vegetableList, setVegetableList] = useState<Item[]>([]);
  const [activeItem, setActiveItem] = useState<Item | null>(null);

  const repeats: string[] = [];
  let i = 0;

  while (i < itemList.length) {
    const itemName: string = itemList[i].name || "";
    if (repeats.includes(itemName)) {
      itemList.splice(i, 1);
    } else {
      repeats.push(itemName);
      i++;
    }
  }

  useEffect(() => {
    let timeoutId: ReturnType<typeof setTimeout> | null = null;

    if (activeItem) {
      timeoutId = setTimeout(() => {
        moveItemBack(activeItem);
        setActiveItem(null);
      }, 5000);
    }

    return () => {
      if (timeoutId) {
        if (itemList.length === 0) {
          setActiveItem(null);
        }
      }
    };
  }, [activeItem, itemList]);

  const moveItem = (item: Item) => {
    if (item.type === "Fruit") {
      setFruitList((prevList) => [...prevList, item]);
    } else if (item.type === "Vegetable") {
      setVegetableList((prevList) => [...prevList, item]);
    }

    setItemList((prevList) => prevList.filter((i) => i.name !== item.name));
    setActiveItem(item);
  };

  const moveItemBack = (item: Item) => {
    setItemList((prevList) => [...prevList, item]);

    if (item.type === "Fruit") {
      setFruitList((prevList) =>
        prevList.filter((fruit) => fruit.name !== item.name)
      );
    } else if (item.type === "Vegetable") {
      setVegetableList((prevList) =>
        prevList.filter((veg) => veg.name !== item.name)
      );
    }
  };

  const Button: React.FC<{ item: Item; onClick: () => void }> = ({
    item,
    onClick,
  }) => (
    <button key={`${item.type}-${item.name}`} onClick={onClick}>
      {item.name}
    </button>
  );

  return (
    <div className="root-container">
      <div className="list-column" id="itemList">
        {itemList.length > 0 &&
          itemList.map((item, index) => (
            <Button
              key={`${item.type}-${item.name}-${index}`}
              item={item}
              onClick={() => moveItem(item)}
            />
          ))}
      </div>
      <div className="list-column" id="fruitColumn">
        <h3>Fruit</h3>
        {fruitList.length > 0 &&
          fruitList.map((item) => (
            <Button
              key={item.name}
              item={item}
              onClick={() => moveItemBack(item)}
            />
          ))}
      </div>
      <div className="list-column" id="vegetableColumn">
        <h3>Vegetable</h3>
        {vegetableList.length > 0 &&
          vegetableList.map((item) => (
            <Button
              key={item.name}
              item={item}
              onClick={() => moveItemBack(item)}
            />
          ))}
      </div>
    </div>
  );
};

export default App;
