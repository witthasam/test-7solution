import { FC, useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";

interface Address {
  postalCode: string;
}

interface Hair {
  color: string;
}

interface User {
  id: number;
  firstName: string;
  lastName: string;
  gender: "Male" | "Female";
  age: number;
  hair: Hair;
  address: Address;
  department: string;
}

interface DepartmentSummary {
  male: number;
  female: number;
  ageRange: string;
  hair: Record<string, number>;
  addressUser: Record<string, string>;
}

interface DataResponse {
  [department: string]: DepartmentSummary;
}

interface ApiResponse {
  users: User[];
}

const App: FC = () => {
  const [transformedData, setTransformedData] = useState<string | null>(null);

  const FetchDataFromApi = async (): Promise<User[]> => {
    try {
      const response: AxiosResponse<ApiResponse> = await axios.get<ApiResponse>(
        "https://dummyjson.com/users"
      );
      return response.data.users;
    } catch (error) {
      console.error("Error fetching data from API:", (error as Error).message);
      return [];
    }
  };

  const TransformData = (users: User[]): DataResponse => {
    const result: DataResponse = {};

    users.forEach((user) => {
      const { department, gender, age, hair, address, firstName, lastName } =
        user;

      if (!result[department]) {
        result[department] = {
          male: 0,
          female: 0,
          ageRange: `${age}-${age}`,
          hair: {},
          addressUser: {},
        };
      }

      if (gender && gender.toLowerCase() === "male") {
        result[department].male++;
      } else if (gender && gender.toLowerCase() === "female") {
        result[department].female++;
      }

      const [minAge, maxAge] = result[department].ageRange
        .split("-")
        .map(Number);

      if (!result[department].ageRange || age < minAge) {
        result[department].ageRange = `${age}-${maxAge}`;
      }

      if (!result[department].ageRange || age > maxAge) {
        result[department].ageRange = `${minAge}-${age}`;
      }

      if (hair) {
        const hairColor = hair.color || "";
        result[department].hair[hairColor] =
          (result[department].hair[hairColor] || 0) + 1;
      }

      result[department].addressUser[`${firstName} ${lastName}`] =
        address.postalCode || "";
    });

    return result;
  };

  const Main = async () => {
    const users = await FetchDataFromApi();
    if (users.length > 0) {
      const data = TransformData(users);
      setTransformedData(JSON.stringify(data, null, 2));
    } else {
      console.log("No data available.");
    }
  };

  useEffect(() => {
    Main();
  }, []);

  return <pre>{transformedData}</pre>;
};

export default App;
